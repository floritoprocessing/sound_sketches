class Clock {
  
  private final long holdTime;
  
  private final float dispX = 50;
  private final float dispY = 110;
  private final float dispRad = 40;

  private long startTime = 0;
  private boolean running = false;
  private boolean done = false;
  
  private DeepMode deepMode;
  
  Clock(long holdTime) {
    this.holdTime = holdTime;
  }
  
  void linkDeepMode(DeepMode deepMode) {
    this.deepMode = deepMode;
  }
  
  boolean started() {
    return running || done;
  }
  
  float getTimePercentage() {
    return max(0,min(1, (float)(System.currentTimeMillis()-startTime)/holdTime));
  }
  
  void start() {
    running = true;
    done = false;
    startTime = System.currentTimeMillis();
  }
  
  void stop() {
    running = false;
    done = false;
    deepMode.setActive(false);
  }
  
  void update() {
    if (running && System.currentTimeMillis()>startTime+holdTime) {
      println("FIRE!");
      running = false;
      done = true;
      deepMode.setActive(true);
    }
  }
  
  void draw() {
    
    if (running || done) {
      textAlign(CENTER);
      long ti = (System.currentTimeMillis()-startTime);
      
      if (!done) {
        text("Time: "+ti,dispX,dispY+dispRad+20);
      } else {
        text("DONE",dispX,dispY+dispRad+20);
      }
      
      float perc = min(1, (float)ti/holdTime);
      
      if (done) {
        perc = 1;
      }
      
      fill(255,0,0);
      arc(dispX,dispY,dispRad,dispRad, 1.5*PI, 1.5*PI + TWO_PI*perc);
    }
    
    ellipseMode(RADIUS);
    stroke(running ? 255 : 128);
    noFill();
    ellipse(dispX,dispY,dispRad,dispRad);
    ellipse(dispX,dispY,2,2);
    line(dispX,dispY,dispX,dispY-dispRad);
  }
  
}
