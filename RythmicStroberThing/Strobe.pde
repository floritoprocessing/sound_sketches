class Strobe {
  
  private final int cycleTime;
  
  private long lastOn;
  private boolean on = false;
  
  Strobe(float bpm) {
    cycleTime = (int)(60000/bpm);
    lastOn = System.currentTimeMillis();
    println(cycleTime+" "+lastOn);
    on = true;
  }
  
  void update() {
    long ms = System.currentTimeMillis();
    
    if (ms>lastOn+cycleTime) {
      on = true;
      lastOn = ms;
      //print(frameCount+" ");
    } else {
      on = false;
    }
    
    
  }
  
  void draw() {
    if (on) {
      background(255);
    }
  }
  
}
