class BeatMaker {
  
  private final int resetTime = 1000;  
  private final float hitLengthForTempo = 1.0 / 8;
  private final float averagingFac = 0.8;
  
  private long lastTime = 0;
  private long deltaTime = 0;
  private float bpm = 0;
  
  private final float BPM;
  private final float BPM_off;
  
  private final float dispX = 10;
  private final float dispY = 20;
  private final float dispWidth = 500;
  private final float dispHeight = 20;
  
  private final float dispBpmRange = 240;
  
  private Clock clock;
  
  BeatMaker(float BPM, float BPM_off) {
    this.BPM = BPM;
    this.BPM_off = BPM_off;
  }
  
  void linkClock(Clock clock) {
    this.clock = clock;
  }
  
  void update() {
    long ms = System.currentTimeMillis();
    
    if (ms>lastTime+resetTime 
    || bpm==Float.NEGATIVE_INFINITY 
    || bpm==Float.POSITIVE_INFINITY 
    || bpm==Float.NaN ) {
      lastTime=0;
      deltaTime=0;
      bpm=0;
      clock.stop();
    }
    
  }
  
  boolean isActive() {
    return (bpm!=0);
  }
  
  boolean inRange() {
    return bpm >= BPM-BPM_off && bpm < BPM+BPM_off;
  }
  
  void hit() {
    long ms = System.currentTimeMillis();//millis();
    
    update();
    
    if (lastTime!=0) {
      deltaTime = ms-lastTime;
      float tBpm = 60000.0/deltaTime * (hitLengthForTempo/0.25);
      if (bpm==0) {
        bpm = tBpm;
      } else {
        bpm = averagingFac*bpm + (1-averagingFac)*tBpm;
      }
      
      if (inRange()) {
        if (!clock.started()) {
          clock.start();
        }
      } else {
        clock.stop();
      }
      
    }
    
    lastTime = ms;
  }
  
  void draw() {
    rectMode(CORNER);
    if (bpm!=0) {
      noStroke();
      fill(255,0,0);
      rect(dispX, dispY, bpm/dispBpmRange*dispWidth, dispHeight);
      fill(255);
      textAlign(LEFT);
      text("BPM: "+beatMaker.bpm,dispX+5,dispY+14);
    }
    
    stroke(isActive() ? 255 : 128);
    noFill();
    rect(dispX,dispY,dispWidth,dispHeight);
    
    float sx = dispX + BPM/dispBpmRange*dispWidth;
    line(sx, dispY, sx, dispY+dispHeight);
    fill(isActive() ? 255 : 128);
    textAlign(CENTER);
    text(""+BPM,sx, dispY+dispHeight+15);
    
    sx = dispX + (BPM+BPM_off)/dispBpmRange*dispWidth;
    stroke(128);
    line(sx, dispY, sx, dispY+dispHeight);
    
    sx = dispX + (BPM-BPM_off)/dispBpmRange*dispWidth;
    stroke(128);
    line(sx, dispY, sx, dispY+dispHeight);
  }
  
}
