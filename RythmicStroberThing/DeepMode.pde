class DeepMode {
  
  private final float dispX = 10;
  private final float dispY = 200;
  
  private boolean active = false;
  
  DeepMode() {
  }
  
  void draw() {
    rectMode(CORNER);
    stroke(active ? 255 : 128);
    if (active) {
      fill(255,0,0);
    } else {
      noFill();
    }
    rect(dispX, dispY, 100, 20);
  }
  
  void setActive(boolean active) {
    this.active = active;
  }
  
  boolean isActive() {
    return active;
  }
  
}
