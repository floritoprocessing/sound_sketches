import krister.Ess.*;
import processing.opengl.*;

final float TARGET_BPM = 153.378; //Loop2_153.378.wav
final float BPM_RANGE = 10;

final int BEAT_HOLD_TIME = 3000;

final String basicLoopName = "Loop2_153.378_3min.wav";

final float HS_HIGH = 20000;
final float HS_LOW = 1000;
final float HS_GAIN = -36;

BeatMaker beatMaker = new BeatMaker(TARGET_BPM, BPM_RANGE);
Clock clock = new Clock(BEAT_HOLD_TIME);
DeepMode deepMode = new DeepMode();
Strobe strobe = new Strobe(TARGET_BPM*2);

AudioStream myStream;
AudioFile myFile;
HighShelf highShelf;
float hsFreq = HS_HIGH;
Amplify amp;
//Normalize

void setup() {
  size(1024,768,OPENGL);
  background(0);
  smooth();
  textFont(loadFont("Verdana-Bold-12.vlw"));
  
  beatMaker.linkClock(clock);
  clock.linkDeepMode(deepMode);
  
  Ess.start(this);
  
  myFile=new AudioFile(basicLoopName,0,Ess.READ);

  // create a new AudioStream and set the sample rate
  myStream=new AudioStream(4*1024); // 128k buffer
  myStream.sampleRate(myFile.sampleRate);
  myStream.start();


  highShelf = new HighShelf(hsFreq, HS_GAIN);//LowPass(20000, -18, 1);
  amp = new Amplify(0.5);


}

void keyPressed() {
  beatMaker.hit();
}



void audioStreamWrite(AudioStream theStream) {
  // read the next chunk
  //System.arraycopy(streamBuffer,0,myStream.buffer,0,streamBuffer.length);
  
  
  
  int samplesRead=myFile.read(myStream);
  if (samplesRead==0) {
    myFile.close();
    myFile.open(basicLoopName,myStream.sampleRate,Ess.READ);
    samplesRead=myFile.read(myStream);
  }
  //println(samplesRead);
  amp.filter(myStream);
  highShelf.filter(myStream);
  
}



void draw() {
  
  if (myStream.state == Ess.STOPPED) {
    myStream.start();
  }
  
  beatMaker.update();
  clock.update();
  strobe.update();
  
  if (clock.started()) {
    float p = clock.getTimePercentage();
    float exp10_0 = log(HS_HIGH)/log(10);
    float exp10_1 = log(HS_LOW)/log(10);
    highShelf.frequency = pow(10,exp10_0 + p*(exp10_1-exp10_0));
  } else {
    highShelf.frequency = HS_HIGH;
  }
  
  //deepMode.update();
  
  
  
  background(0);
  beatMaker.draw();
  clock.draw();
  deepMode.draw();
  
  if (deepMode.isActive()) {
    strobe.draw();
  }
  
}


public void stop() {
  Ess.stop();
  super.stop();
}
