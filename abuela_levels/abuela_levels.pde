import Sonia.*;

Sample abuela;
int nrOfSamples, nrOfFrames;
float[] avLevels;

void setup() {
  size(1200,200); background(0);
  Sonia.start(this,44100);
  // load sound
  abuela=new Sample("abuela.aif");

  // put audio data into float array
  nrOfSamples=abuela.getNumFrames();
  println("number of frames: "+nrOfSamples);
  float[] data=new float[nrOfSamples];
    
  print("reading data into array.. ");
  abuela.read(data);
  println("done");

  
  // chop audio into pieces and get average levels
  int nrOfSamplesPerFrame=44100/25; println("Nr of samples per frame: "+nrOfSamplesPerFrame);
  nrOfFrames=nrOfSamples/nrOfSamplesPerFrame; println("Nr of frames: "+nrOfFrames);
  avLevels=new float[nrOfFrames];
  
  for (int frame=0;frame<nrOfFrames;frame++) {
    double av_level=0.0;
    int sampleCount=0;
    for (int sample=0;sample<nrOfSamplesPerFrame;sample++) {
      int absSample=frame*nrOfSamplesPerFrame+sample;
      if (absSample<nrOfSamples) {
        av_level+=abs(data[absSample]);
        sampleCount++;
      }
    }
    avLevels[frame]=float(av_level/(float)sampleCount);
    if (frame>=800&&frame<1200) {println("level["+frame+"]="+av_level/(float)sampleCount+"; ");}
//    if (frame>=0500&&frame<1000) {println("level["+frame+"]="+av_level/(float)sampleCount+"; ");}
//    if (frame>=1000&&frame<1500) {println("level["+frame+"]="+av_level/(float)sampleCount+"; ");}
  }
  
  // draw average audio on screen
  abuela.play();  
}

void loop() {
  background(0); 
  
  // draw total average levels
  stroke(128,128,128);
  beginShape(LINE_STRIP);
  for (int i=0;i<nrOfFrames;i++) {
    vertex(1200*i/(float)nrOfFrames,200-avLevels[i]*600);
  }
  endShape();
  
  // draw playhead
  stroke(64,64,255);
  float x=1200*abuela.getCurrentFrame()/(float)nrOfSamples;
  line(x,0,x,200);
  
  // draw waveform as in the application 580x74
  rectMode(CENTER_DIAMETER); stroke(255,255,255); noFill();
  rect(600,100,580,74);
  
  int framePos=int(nrOfFrames*abuela.getCurrentFrame()/(float)nrOfSamples);
  beginShape(LINE_STRIP);
  for (int i=0;i<=58*2;i++) {
    vertex(600-290+i*5,100+avLevels[framePos]*random(-200,200));
  }
  endShape();
}

public void stop(){ 
  abuela.stop();
  Sonia.stop();
  super.stop(); 
} 
