//celesta: C3 - C8 [61]
Sampler Celesta;

void setup() {
  Sonia.start(this,44100);
  Celesta=new Sampler(); Celesta.initSounds();
}

void loop() {
}

void mousePressed() {
  Celesta.playSample((int)random(61),0.3,random(-1,1));
}

public void stop(){ 
  Sonia.stop(); 
//  super.stop(); 
} 

class Sampler {
  String[] tone={"C","Ci","D","Di","E","F","Fi","G","Gi","A","Ai","H"};
  Sample[] sample;
  
  Sampler() {}
  
  void initSounds() {
    sample=new Sample[61];
    for (int i=0;i<61;i++) {
      int okt=3+int(floor(i/12));
      String name="CEL "+tone[i%12]+""+okt+".aif"; println(name+" | ");
      sample[i]=new Sample(name);
    }
  }
  
  void playSample(int nr, float vol, float pan) {
    sample[nr].setVolume(vol);
    sample[nr].setPan(pan);
    sample[nr].play();
  }
}
