import java.awt.Component;
import java.util.ArrayList;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiDevice.Info;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.Transmitter;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;


public class MidiManager {

	private Info[] deviceInfos;
	//private Info[] deviceInfosIn, deviceInfosOut;

	public MidiManager(final Component parentComponent) {
		
		
		deviceInfos = MidiSystem.getMidiDeviceInfo();
		ArrayList in = new ArrayList();
		ArrayList out = new ArrayList();
		for (int i=0;i<deviceInfos.length;i++) {
			//System.out.println("Device "+deviceInfos[i].getName());
			MidiDevice device = null;
			try {
				device = MidiSystem.getMidiDevice(deviceInfos[i]);
			} catch (MidiUnavailableException e) {
				e.printStackTrace();
			}
			if (device!=null) {
				System.out.println(device+" "+device.getClass());
				if (device instanceof Transmitter) {
					in.add(deviceInfos[i]);
				}
				else if (device instanceof Receiver) {
					out.add(deviceInfos[i]);
				}
			}
		}
		//deviceInfosIn = in.toArray(new Info[0]);
		//deviceInfosOut = out.toArray(new Info[0]);
		System.out.println("IN: "+in.size());
		System.out.println("OUT: "+out.size());
		
		final String[] deviceInNames = new String[in.size()];
		for (int i=0;i<deviceInNames.length;i++) {
			deviceInNames[i] = ((Info)in.get(i)).getName();
			System.out.println("in "+i+": "+deviceInNames[i]);
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JOptionPane.showOptionDialog(parentComponent, 
						"Choose Midi In", "Choose Midi In", 
						JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, 
						null, deviceInNames, null);
			}
		});
	}
	
	
	public static void main(String[] args) {
		
		JFrame frame = new JFrame();
		frame.setVisible(true);
			
		MidiManager mm = new MidiManager(frame);
	}
	
}
