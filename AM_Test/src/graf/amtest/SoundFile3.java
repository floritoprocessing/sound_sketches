package graf.amtest;

import jm.util.*;

//http://jmusic.ci.qut.edu.au/jmtutorial/ReadWriteAudioFiles.html

public class SoundFile3 {

	private float[] data = new float[1];
	
	public void load(String filename) {
		data = Read.audio(filename);
	}
	
	public void save(String filename) {
		Write.audio(data, filename);
	}

	public int getLength() {
		return data.length;
	}
	
	public void setLength(int n) {
		float[] newData = new float[n];
		int amount = Math.min(data.length,n);
		System.arraycopy(data, 0, newData, 0, amount);
		data = newData;
	}
	
	public void setValueAt(int i, float val) {
		data[i] = val;
	}

	public float getValueAt(int i) {
		return data[i];
	}

	public void resample(float srSource, float srTarget) {
		int newLength = (int)(data.length/srSource*srTarget);
		float[] newData = new float[newLength];
		/*if (srSource<srTarget) {
			for (int targetIndex=0;targetIndex<newLength;targetIndex++) {
				int i0 = (int)((float)targetIndex/srTarget*srSource);
				int i1 = i0+1;
				double p = (double)targetIndex/srTarget*srSource - i0;
				if (i1<data.length)
					newData[targetIndex] = (float)(data[i0]+p*(data[i1]-data[i0]));
				else
					newData[targetIndex] = (float)data[i0];
			}
		} else {*/
			for (int targetIndex=0;targetIndex<newLength;targetIndex++) {
				int i0 = (int)((float)targetIndex/srTarget*srSource);
				newData[targetIndex] = data[i0];
			}
		//}
		data = newData;
	}

}
