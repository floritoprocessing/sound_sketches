package graf.amtest;

import java.io.*;

import javax.sound.sampled.*;

public class SoundFile2 {

	private int bytesPerFrame;
	private long length;
	private int channels;
	private float sampleRate;
	private byte[] audioBytes;
	
	public void loadConvertSave(String filenameIn, String filenameOut) {
		File fileIn = new File(filenameIn);
		try {
			
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(fileIn);
			bytesPerFrame = audioInputStream.getFormat().getFrameSize();
			length = audioInputStream.getFrameLength();
			channels = audioInputStream.getFormat().getChannels();
			sampleRate = audioInputStream.getFormat().getSampleRate();
			if (bytesPerFrame == AudioSystem.NOT_SPECIFIED) throw new RuntimeException("Unknown bytes per frame");
			System.out.println(bytesPerFrame+" bytes per frame");
			System.out.println(channels+" channel(s)");
			System.out.println(sampleRate+" sample rate");
			System.out.println("frames: "+length);

			File fileOut = new File(filenameOut);
			FileOutputStream outputStream = new FileOutputStream(fileOut);
			//AudioSystem.
			
			int totalFramesRead = 0;
			// Set an arbitrary buffer size of 1024 frames.
			int numBytes = 1024 * bytesPerFrame; 
			audioBytes = new byte[numBytes];

			int numBytesRead = 0;
			int numFramesRead = 0;
			// Try to read numBytes bytes from the file.
			while ((numBytesRead = audioInputStream.read(audioBytes)) != -1) {
				// Calculate the number of frames actually read.
				numFramesRead = numBytesRead / bytesPerFrame;
				totalFramesRead += numFramesRead;
				// Here, do something useful with the audio data that's 
				// now in the audioBytes array...
				//AudioSystem.write()
				
			}
			System.out.println(totalFramesRead);

		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void save(String string) {
		// TODO Auto-generated method stub

	}

	public int getLength() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getValueAt(int i) {
		// TODO Auto-generated method stub
		return 0;
	}

}
