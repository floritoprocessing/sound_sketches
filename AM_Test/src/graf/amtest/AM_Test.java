package graf.amtest;

import processing.core.*;
import graf.oscillator.*;

public class AM_Test extends PApplet {

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.amtest.AM_Test"});
	}

	Oscillator carrierOsc = Oscillator.createSingle(Oscillator.SINE, 24.3);
	Oscillator signal = Oscillator.createSingle(Oscillator.SAMPLE_AND_HOLD, 0.3);
	double am = 0;
	Oscillator fucker = Oscillator.createSingle(Oscillator.NOISE,0);
	double fuckerAmp;
	double AMsignalFucked = 0;
	double lastDem1=0, dem1 = 0;
	double lastDem2=0, dem2=0;

	float[] carrierDraw, signalDraw, amDraw, fuckerDraw, AMSignalFuckedDraw, dem1Draw, dem2Draw;

	SoundFile3 music, carrier;
	
	public void settings() {
		size(640,640,P3D);
	}
	public void setup() {
		
		//smooth();
		carrierDraw = new float[width];
		signalDraw = new float[width];
		amDraw = new float[width];
		fuckerDraw = new float[width];
		AMSignalFuckedDraw = new float[width];
		dem1Draw = new float[width];
		dem2Draw = new float[width];
		
		
		//Oscillator carrierForMusic = new SineOscillator(500000);
		
		music = new SoundFile3();//44100,1);
		music.load("TheYears.wav");
		//music.load("C:\\Documents and Settings\\mgraf\\My Documents\\Processing Eclipse\\AM_Test\\CebesMono.wav");
		System.out.println(music.getLength());
		
		
		int SAMPLE_RATE_UP = 2000000;
		double SAMPLE_RATE_UP_FRAME_LENGTH = 1.0/SAMPLE_RATE_UP;
		int CARRIER_FREQ =    5000;
		
		/*
		 * Sample music up
		 */
		music.resample(44100,SAMPLE_RATE_UP);
		System.out.println(music.getLength());
		
		/*
		 * Create carrier
		 */
		
		SineOscillator sineOscillator = new SineOscillator(CARRIER_FREQ);
		carrier = new SoundFile3();
		carrier.setLength(music.getLength());
		for (int i=0;i<carrier.getLength();i++) {
			carrier.setValueAt(i, (float)sineOscillator.getAmplitude());
			sineOscillator.integrate(SAMPLE_RATE_UP_FRAME_LENGTH);
		}
		
		double demodulated = 0;
		double average = 0;
		for (int i=0;i<carrier.getLength();i++) {
			am = (music.getValueAt(i)*0.5+0.5) * carrier.getValueAt(i);
			//fuckerAmp = 0.1*Math.random();
			AMsignalFucked = 0.95*am + 0.05*Math.random();
			demodulated = demodulated*0.99 + 0.01*Math.abs(AMsignalFucked);
			demodulated = Math.max(Math.min(1,demodulated),0);
			average += demodulated;
			music.setValueAt(i,(float)demodulated);
		}
		average /= carrier.getLength();
		System.out.println(average);
		
		for (int i=0;i<carrier.getLength();i++) {
			music.setValueAt(i,music.getValueAt(i)-(float)average);
			music.setValueAt(i,2*music.getValueAt(i));
		}
		
		//for (int i=0;i<music.getLength();i++)
			//music.setValueAt(i, music.getValueAt(i));
		
		/*
		 * SAMPLE ALL DOWN
		 */		
		
		/*for (int i=0;i<music.getLength();i++) 
			System.out.println(music.getValueAt(i));*/
		
		carrier.resample(SAMPLE_RATE_UP, 44100);
		System.out.println(carrier.getLength());
		
		music.resample(SAMPLE_RATE_UP,44100);
		System.out.println(music.getLength());
		
		println("saving");
		music.save("TheYearsOut1.wav");
		//music.save("C:\\Documents and Settings\\mgraf\\My Documents\\Processing Eclipse\\AM_Test\\CebesMonoOut.wav");
		//carrier.save("C:\\Documents and Settings\\mgraf\\My Documents\\Processing Eclipse\\AM_Test\\Carrier.wav");
	}

	public void draw() {
		background(255);

		for (int i=0;i<10;i++) {
			carrierOsc.integrate(0.01);
			signal.integrate(0.01);
			fucker.integrate(0.01);

			am = (signal.getAmplitude()+1)*0.5 * carrierOsc.getAmplitude();

			fuckerAmp = 0.1*Math.random();
			AMsignalFucked = am + fuckerAmp*fucker.getAmplitude();

			/*
			 * Method: Product Detector
			 * @see http://en.wikipedia.org/wiki/Product_detector
			 * @see http://en.wikipedia.org/wiki/Demodulation
			 */
			lastDem1 = dem1;
			dem1  = 2*AMsignalFucked/carrierOsc.getAmplitude() - 1;
			if (Double.isNaN(dem1) || Double.isInfinite(dem1) || dem1>1 || dem1<-1) dem1=lastDem1;

			dem2=dem2*0.95 + 0.05*Signal.rectify(AMsignalFucked);


		}


		System.arraycopy(carrierDraw, 0, carrierDraw, 1, carrierDraw.length-1);
		carrierDraw[0] = 25*(float)carrierOsc.getAmplitude();
		System.arraycopy(signalDraw, 0, signalDraw, 1, signalDraw.length-1);
		signalDraw[0] = 25*(float)signal.getAmplitude();
		System.arraycopy(amDraw, 0, amDraw, 1, amDraw.length-1);
		amDraw[0] = 25*(float)am;
		System.arraycopy(fuckerDraw, 0, fuckerDraw, 1, fuckerDraw.length-1);
		fuckerDraw[0] = 25*(float)(fucker.getAmplitude()*fuckerAmp);
		System.arraycopy(AMSignalFuckedDraw, 0, AMSignalFuckedDraw, 1, AMSignalFuckedDraw.length-1);
		AMSignalFuckedDraw[0] = 25*(float)AMsignalFucked;
		System.arraycopy(dem1Draw, 0, dem1Draw, 1, dem1Draw.length-1);
		dem1Draw[0] = 25*(float)dem1;
		System.arraycopy(dem2Draw, 0, dem2Draw, 1, dem2Draw.length-1);
		dem2Draw[0] = 25*(float)(2*dem2-1);


		//System
		//dem2Draw[0] = 25*(float)dem2;

		stroke(0,0,0,128);
		for (int x=0;x<width-1;x++) {
			line(x,50-carrierDraw[x],x+1,50-carrierDraw[x+1]);
			line(x,100-signalDraw[x],x+1,100-signalDraw[x+1]);
			line(x,200-amDraw[x],x+1,200-amDraw[x+1]);
			line(x,250-fuckerDraw[x],x+1,250-fuckerDraw[x+1]);
			line(x,300-AMSignalFuckedDraw[x],x+1,300-AMSignalFuckedDraw[x+1]);
			line(x,400-dem1Draw[x],x+1,400-dem1Draw[x+1]);
			line(x,450-dem2Draw[x],x+1,450-dem2Draw[x+1]);
		}
		
		/*for (int i=0;i<music.getLength()-1;i++) {
			int i0 = i;
			int i1 = i+1;
			float y0 = music.getValueAt(i0);
			float y1 = music.getValueAt(i1);
			float x = width*i/(float)music.getLength();
			line(x,500-y0*100,x+1,500-y1*100);
		}*/
	}

	private double[] lowpass(double[] x, double timeInterval, double timeConstant) {
		double[] y = new double[x.length];
		double alpha = timeInterval / (timeConstant+timeInterval);
		y[0]=x[0];
		for (int i=1;i<x.length;i++) {
			y[i] = alpha * x[i] + (1-alpha) * y[i-1];
		}
		return y;
	}

}
