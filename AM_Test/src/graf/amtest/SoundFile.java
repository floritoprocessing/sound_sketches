package graf.amtest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class SoundFile {

	private int sampleRate;
	private int channels;
	private int[] data;

	public SoundFile(int sampleRate, int channels) {
		this.sampleRate = sampleRate;
		this.channels = channels;
	}
	
	public SoundFile(String filename, int sampleRate, int channels) {
		this.sampleRate = sampleRate;
		this.channels = channels;
		load(filename);
	}
	
	public void load(String filename) {
		File file = new File(filename);

		data = new int[(int)(file.length()/2/channels)];

		System.out.println(file.getName()+" has "+data.length+" samples = "+data.length/(double)sampleRate+" seconds");

		try {
			FileInputStream input = new FileInputStream(file);
			byte[] b = new byte[data.length*2];
			input.read(b);
			for (int i=0;i<data.length;i++) {
				int LSB = signedByteToInt(b[2*i]);
				int MSB = signedByteToInt(b[2*i+1]);
				data[i] = MSB<<8|LSB;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void save(String filename) {
		File file = new File(filename);
		try {
			FileOutputStream output = new FileOutputStream(file);
			byte[] b = new byte[data.length*2];
			for (int i=0;i<data.length;i++) {
				int MSB = data[i]>>8&0xff;
				int LSB = data[i]&0xff;
				b[2*i] = intToSignedByte(LSB);
				b[2*i+1] = intToSignedByte(MSB);
			}
			output.write(b);
			output.flush();
			output.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static int unsignedByteToInt(byte b) {
		return (int) b & 0xFF;
	}
	
	private static int signedByteToInt(byte b) {
		int i = (int)b;
		if (i>0) return i;
		else return 255;
	}
	
	private static byte intToSignedByte(int i) {
		if (i<128) return (byte)i;
		else return (byte)(i-255);
	}


	public int getLength() {
		return data.length;
	}

	public int getValueAt(int i) {
		return data[i];
	}

}
