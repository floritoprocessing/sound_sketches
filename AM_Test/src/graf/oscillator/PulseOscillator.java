package graf.oscillator;

public class PulseOscillator extends Oscillator {
	
	private double pulseWidth = 0.5;
	
	public PulseOscillator(double frequency) {
		super(frequency);
	}

	public PulseOscillator(double frequency, double phase) {
		super(frequency, phase);
	}

	public PulseOscillator(double frequency, double phase, double pulseWidth) {
		super(frequency, phase);
		setPulseWidth(pulseWidth);
	}
	
	public void setPulseWidth(double pulseWidth) {
		if (pulseWidth<0 || pulseWidth>1) throw new RuntimeException("pulseWidth value has to be: 0..1");
		this.pulseWidth = pulseWidth;
	}

	public double getAmplitude() {
		if (getCurrentPhase()<pulseWidth) return 1;
		else return -1;
	}

	public String toString() {
		return ("Pulse Oscillator f="+frequency+", phase="+phase+", pulseWidth="+pulseWidth);
	}

}
