package graf.oscillator;

public class SampleAndHoldOscillator extends Oscillator {

	private double y=0;
	
	public SampleAndHoldOscillator(double frequency) {
		super(frequency);
		newFutureAmplitude();
	}

	public SampleAndHoldOscillator(double frequency, double phaseOffset) {
		super(frequency, phaseOffset);
		newFutureAmplitude();
	}
	
	public void integrate(double time) {
		phase += (time*frequency);
		while (phase+phaseOffset>1) {
			phase-=1;
			newFutureAmplitude();
		}
	}
	
	private void newFutureAmplitude() {
		y = Math.random()*2-1;
	}
	
	public double getAmplitude() {
		return y;
	}

	public String toString() {
		return "Noise-Ramp Oscillator";
	}

}
