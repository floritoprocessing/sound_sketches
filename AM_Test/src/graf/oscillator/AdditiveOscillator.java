package graf.oscillator;

public class AdditiveOscillator extends Oscillator {

	private Oscillator[] oscillators;
	private double[] ratios;
	
	public AdditiveOscillator() {
		super();
	}
	
	public AdditiveOscillator(Oscillator[] oscillators, double[] ratios) {
		super();
		this.oscillators = oscillators;
		this.ratios = ratios;
	}
	
	public void addOscillator(Oscillator oscillator, double ratio) {
		Oscillator[] newOscs = new Oscillator[oscillators.length+1];
		System.arraycopy(oscillators, 0, newOscs, 0, oscillators.length);
		newOscs[newOscs.length-1] = oscillator;
		oscillators = newOscs;
		double[] newRatios = new double[ratios.length+1];
		System.arraycopy(ratios, 0, newRatios, 0, ratios.length);
		newRatios[newRatios.length-1] = ratio;
		ratios = newRatios;
	}

	public double getAmplitude() {
		double amplitude = 0;
		for (int i=0;i<oscillators.length;i++) {
			amplitude += ratios[i] * oscillators[i].getAmplitude();
		}
		return amplitude;
	}
	
	public void integrate(double time) {
		for (int i=0;i<oscillators.length;i++)
			oscillators[i].integrate(time);
	}

	public String toString() {
		String out = "Additive Oscillator with:\r\n";
		for (int i=0;i<oscillators.length;i++) {
			out += "\t"+(i+1)+"/"+oscillators.length+": "+oscillators[i].toString()+" at an amplitude of "+ratios[i]+"\r\n";
		}
		return out;
	}

}
