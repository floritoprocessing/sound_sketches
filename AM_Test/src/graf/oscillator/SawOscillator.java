package graf.oscillator;

public class SawOscillator extends Oscillator {

	public SawOscillator(double frequency) {
		super(frequency);
	}
	
	public SawOscillator(double frequency, double phase) {
		super(frequency,phase);
	}

	public double getAmplitude() {
		double p = getCurrentPhase();
		if (p<0.5) return p*2;
		return p*2-2;
	}
	
	public String toString() {
		return ("Saw Oscillator f="+frequency+", phase="+phase);
	}

}
