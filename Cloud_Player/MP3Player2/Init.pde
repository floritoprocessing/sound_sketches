void loadNewFile() {
  // get a new random file
  File file = null;
  do {
    int index = (int)(Math.random()*list.size());
    file = list.get(index);
  } 
  while (filesPlaying.contains (file));

  // load file
  println("Loading "+file.getName());
  AudioPlayer player = minim.loadFile(file.getAbsolutePath());
  players.add(player);
  player.play();
  
  println(player.getControls());
  //player.setVolume(0);
  //player.shiftVolume(1,0,10000);
}
