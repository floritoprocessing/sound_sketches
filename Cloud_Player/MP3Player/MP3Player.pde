import ddf.minim.*;

ArrayList filesPlaying = new ArrayList();
ArrayList players = new ArrayList();

Minim minim;
MP3List list;
int bufferSize = 1024;

void setup() {
  size(512, 500, P3D);
  minim = new Minim(this);


  println("Loading mp3 list..");
  list = new MP3List("D:\\- Party40\\Audio");
  println("done ("+list.size()+" files)");
}

void loadNewFile() {
  // get a new random file
  File file = null;
  do {
    int index = (int)(Math.random()*list.size());
    file = list.get(index);
  } 
  while (filesPlaying.contains (file));

  // load file
  println("Loading "+file.getName());
  AudioPlayer player = minim.loadFile(file.getAbsolutePath());
  players.add(player);

  player.play();
}

void mousePressed() {
  
  if (mouseButton==RIGHT){ 
    println("skipping");
    //for (int i=0;i<players.size();i++) {
    int i = (int)(random(players.size()));
      AudioPlayer player = (AudioPlayer)players.get(i);
      if (player.isPlaying()) {
        int timeRemaining = player.length() - player.position();
        println("rem "+timeRemaining);
        player.skip(timeRemaining/2);
      }
    //}
  }
  
}

void draw() {
  background(0);
  noStroke();
  
 
  if (mousePressed && mouseButton==LEFT) {
    loadNewFile();
  }

  // remove stopped files:
  for (int i=players.size()-1;i>=0;i--) {
    AudioPlayer player = (AudioPlayer)players.get(i);
    if (!player.isPlaying()) {
      players.remove(i);
    }
  }

  for (int i=0;i<players.size();i++) {
    AudioPlayer player = (AudioPlayer)players.get(i);
    long len = player.length();
    long pos = player.position();

    float y0 = map(len, 0, 6*60000, height, 20);
    float y1 = map(pos, 0, 6*60000, height, 20);
    float x = i*3;
    
    fill(128);
    rectMode(CORNERS);
    rect(x,y0,x+3,height);
    fill(255,0,0);
    rect(x,y1,x+3,height);
  }
  
  fill(255);
  text("Files playing: "+players.size(),10,20);
  
}

