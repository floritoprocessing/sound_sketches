import de.voidplus.leapmotion.*;

import themidibus.*;

import ddf.minim.*;
import java.util.Collections;
import java.util.Comparator;

LeapMotion leap;
String gestureString = "";

MidiBus input;

ArrayList filesPlaying = new ArrayList();
ArrayList<AudioPlayer> players = new ArrayList();

int maxPlayers = 120;

Minim minim;
MP3List list;
int bufferSize = 1024;

// 2 3db
// 4 6
// 8 9
// 16 12
// 32 15
// 64 18
// 128 21

boolean initPhase = true;
float maxGain = -48;



void setup() {
  size(600, 500, P3D);
  minim = new Minim(this);

  println("Loading mp3 list..");
  list = new MP3List("D:\\- Party40\\database audio");
  println("done ("+list.size()+" files)");

  //  for (int i=0;i<maxPlayers;i++) {
  //    AudioPlayer p = loadNewFile();
  //   // p.setBalance(map(i,0,maxPlayers-1,-1,1));
  //  }
  MidiBus.list();
  input = new MidiBus(this, "VMeter 1.30 A", -1);

  leap = new LeapMotion(this).withGestures();
}





void mousePressed() {

  if (mouseButton==RIGHT) { 
    println("skipping");
    //for (int i=0;i<players.size();i++) {
    int i = (int)(random(players.size()));
    AudioPlayer player = (AudioPlayer)players.get(i);
    if (player.isPlaying()) {
      int timeRemaining = player.length() - player.position();
      println("rem "+timeRemaining);
      player.skip(timeRemaining/2);
    }
    //}
  }
}

int screenPosToPlayerIndex(float x) {
  return (int)map(x, 0, width, 0, maxPlayers);
}

float playerIndexToScreenPos(int index) {
  return map(index, 0, maxPlayers, 0, width);
}

int screenPosToPlayerRange(float y) {
  return (int)map(y, height, 0, 0, maxPlayers);
}


void updatePlayers() {
  //if (mouseHasMoved) {
  //mouseHasMoved = false;
  int centerIndex = screenPosToPlayerIndex(mouseX);
  
//  int centerIndex = 0;
//  if (handPositions!=null && handPositions.length>0) {
//    centerIndex = (int)map(handPositions[0].x,-width,2*width,0, maxPlayers);
//  }
  
  //int range = screenPosToPlayerRange(mouseY);
  int range = 0;
  if (totalFingerDistances!=null && totalFingerDistances.length>0) {
    float handStretchPercentage = map(totalFingerDistances[0],80,140,0,1);
    handStretchPercentage = pow(handStretchPercentage,1.1);
    range = (int)map(handStretchPercentage,0,1,0,maxPlayers);
    range = max(0,range);
  }
  
  int i0 = max(centerIndex, 0);
  int i1 = min(centerIndex+range, maxPlayers-1);

  float x0 = playerIndexToScreenPos(i0);
  float x1 = playerIndexToScreenPos(i1);

  rectMode(CORNERS);
  fill(0, 128, 255, 128);
  rect(x0, height/2-2, x1, height/2+2);

//  if (!mousePressed) {
//    i0=maxPlayers;
//    i1=maxPlayers+1;
//  }

  float percentageOfAll = (float)(i1-i0)/maxPlayers;
  //println(percentageOfAll);
  float onGain = map(percentageOfAll, 0, 1, maxGain, 0);

  for (int i=0;i<players.size();i++) {
    AudioPlayer player = (AudioPlayer)players.get(i);
    if (i<i0 || i>=i1) {
      player.setGain(-80);
    } 
    else {
      player.setGain(onGain);
    }
  }
  //}
}




float[] totalFingerDistances;
PVector[] handPositions;

void draw() {
  background(255);
  noStroke();


  //
  //  BEGIN LEAP
  //
  
  String handText = "Hands:\n";
  handText += "  amount = "+leap.getHands().size()+"\n\n";
  
  
  ArrayList<Hand> hands = leap.getHands();
  totalFingerDistances = new float[hands.size()];
  
  
  handPositions = new PVector[hands.size()];
  
  // HANDS
  int handIndex = 0;
  for(Hand hand : hands){

    hand.draw();
    int     hand_id          = hand.getId();
    PVector hand_position    = hand.getPosition();
    PVector hand_stabilized  = hand.getStabilizedPosition();
    PVector hand_direction   = hand.getDirection();
    PVector hand_dynamics    = hand.getDynamics();
    float   hand_roll        = hand.getRoll();
    float   hand_pitch       = hand.getPitch();
    float   hand_yaw         = hand.getYaw();
    float   hand_time        = hand.getTimeVisible();
    PVector sphere_position  = hand.getSpherePosition();
    float   sphere_radius    = hand.getSphereRadius();
    
    //println(hand_position);
    handPositions[handIndex] = hand_position;
    
    handText += "  Hand\n";
    handText += "    id  = "+hand_id+"\n";
    handText += "    pos = "+pVecToText(hand_position)+"\n";
    
    // FINGERS
    handText += "    finger amount = "+hand.getFingers().size()+"\n";
    
    
    totalFingerDistances[handIndex] = 0;
    PVector lastFingerPos = null;
    
    ArrayList<Finger> fingers = hand.getFingers();
    int fAmount = fingers.size();
    if (fAmount>1) {
      
      
      for(int i=0;i<fAmount-1;i++){
        Finger f0 = fingers.get(i);
        PVector p0 = f0.getPosition();
        float shortestDistance = Float.MAX_VALUE;
        for (int j=i+1;j<fAmount;j++) {
          
          Finger f1 = fingers.get(j);
          PVector p1 = f1.getPosition();
          
          float distance = PVector.dist(p0,p1);
          shortestDistance = Math.min(shortestDistance, distance);
          
        }
        totalFingerDistances[handIndex] += shortestDistance;
        //print(shortestDistance+"\t");
      }
     // println();
      
      totalFingerDistances[handIndex] /= (fAmount-1);
    }
    
    
    
    println(handIndex+": "+totalFingerDistances[handIndex]);
    
    for(Finger finger : fingers){
      
      // Basics
      finger.draw();
      int     finger_id         = finger.getId();
      PVector finger_position   = finger.getPosition();
      PVector finger_stabilized = finger.getStabilizedPosition();
      PVector finger_velocity   = finger.getVelocity();
      PVector finger_direction  = finger.getDirection();
      float   finger_time       = finger.getTimeVisible();
      handText += "    id = "+finger_id+", ";
      handText += " pos = "+pVecToText(finger_position)+"\n";
      
      // Touch Emulation
      int     touch_zone        = finger.getTouchZone();
      float   touch_distance    = finger.getTouchDistance();
      
      switch(touch_zone){
        case -1: // None
          break;
        case 0: // Hovering
          //println("Hovering (#"+finger_id+"): "+touch_distance);
          break;
        case 1: // Touching
          //println("Touching (#"+finger_id+")");
          break;
      }
    }
    handText += "\n";
    
    
    // TOOLS
    for(Tool tool : hand.getTools()){
      
      // Basics
      tool.draw();
      int     tool_id           = tool.getId();
      PVector tool_position     = tool.getPosition();
      PVector tool_stabilized   = tool.getStabilizedPosition();
      PVector tool_velocity     = tool.getVelocity();
      PVector tool_direction    = tool.getDirection();
      float   tool_time         = tool.getTimeVisible();
      
      // Touch Emulation
      int     touch_zone        = tool.getTouchZone();
      float   touch_distance    = tool.getTouchDistance();
      
      switch(touch_zone){
        case -1: // None
          break;
        case 0: // Hovering
          // println("Hovering (#"+tool_id+"): "+touch_distance);
          break;
        case 1: // Touching
          //println("Touching (#"+tool_id+")");
          break;
      }
    }
   
   handIndex++; 
  }
  
  // DEVICES
  for(Device device : leap.getDevices()){
    float device_horizontal_view_angle = device.getHorizontalViewAngle();
    float device_verical_view_angle = device.getVerticalViewAngle();
    float device_range = device.getRange();
    handText += "Device: horizontal view angle = "+degrees(device_horizontal_view_angle)+"\n";
    handText += "        vertical view angle   = "+degrees(device_verical_view_angle)+"\n";
    handText += "        range                 = "+device_range+"\n";
  }
  
  
  //handText += "\n";
  //handText += gestureString;
  
  // on screen:
 
  shadowText(handText,10,20);
  shadowText(gestureString,width-textWidth("KeyTapGesture by finger 123..."),20);
  
  
  //
  //  END LEAP
  //

  


  if (initPhase) {
    if (players.size()<maxPlayers) {
      loadNewFile();
    } 
    else {
      Collections.sort(players, new Comparator() {
        public int compare(Object o0, Object o1) {
          AudioPlayer p0 = (AudioPlayer)o0;
          AudioPlayer p1 = (AudioPlayer)o1;
          int l0 = p0.length();
          int l1 = p1.length();
          if (l0<l1) {
            return -1;
          } 
          else if (l0>l1) {
            return 1;
          } 
          else {
            return 0;
          }
        }
      }
      );
      for (int i=0;i<players.size();i++) {
        //println(players.get(i).length());
        players.get(i).setBalance(map(i, 0, players.size()-1, -1, 1));
      }
      initPhase = false;
    }
  }




  // remove stopped files:
  //  for (int i=players.size()-1;i>=0;i--) {
  //    AudioPlayer player = (AudioPlayer)players.get(i);
  //    if (!player.isPlaying()) {
  //      players.remove(i);
  //    }
  //  }


  for (int i=0;i<players.size();i++) {
    AudioPlayer player = (AudioPlayer)players.get(i);
    long len = player.length();
    long pos = player.position();

    float y0 = map(len, 0, 6*60000, height, 20);
    float y1 = map(pos, 0, 6*60000, height, 20);
    float w = (float)width/maxPlayers;
    float x = i*w;

    if (player.isPlaying()) {
      fill(128);
    } 
    else {
      fill(128, 0, 0);
    }

    rectMode(CORNERS);
    rect(x, y0, x+w, height);
    if (player.isPlaying()) {
      fill(0, 128, 0);
      rect(x, y1, x+w, height);
    }
  }

  fill(255);
  text("Files playing: "+players.size(), 10, 20);

  updatePlayers();
}





void appendGestureString(String txt) {
  gestureString += txt+"\n";
  
  int max = 15;
  String[] lines = gestureString.split("\n");
  if (lines.length==max+1) {
    String[] newLines = new String[max];
    arrayCopy(lines, 1, newLines, 0, max);
    lines = newLines;
  }
  
  gestureString = "";
  for (int i=0;i<lines.length;i++) {
    gestureString += lines[i] + "\n";
  }
  
}



String pVecToText(PVector v) {
  return "[ "+nf(v.x,1,1)+", "+nf(v.y,1,1)+", "+nf(v.z,1,1)+" ]"; 
}



void shadowText(String txt, float x, float y) {
  
  fill(128,128);
  text(txt,x+1,y+1);
  fill(192,0,0);
  text(txt,x,y);
  
}
