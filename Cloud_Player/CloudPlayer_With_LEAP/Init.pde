AudioPlayer loadNewFile() {


  // load file

    AudioPlayer player = null;
  do {
    // get a new random file
    File file = null;
    do {
      int index = (int)(Math.random()*list.size());
      file = list.get(index);
    } 
    while (filesPlaying.contains (file));
    println("Loading "+file.getName());
    try {
      player = minim.loadFile(file.getAbsolutePath());
    } 
    catch (NullPointerException e) {
      player = null;
    };
  } 
  while (player==null || player.length ()<=0);
  players.add(player);
  player.play();
  player.setGain(-80);
  player.loop();

  //println(player.getControls());

  return player;
  //player.setVolume(0);
  //player.shiftVolume(1,0,10000);
}

