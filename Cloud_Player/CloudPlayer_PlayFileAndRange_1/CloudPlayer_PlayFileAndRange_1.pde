import themidibus.*;

import ddf.minim.*;
import java.util.Collections;
import java.util.Comparator;

MidiBus input;

ArrayList filesPlaying = new ArrayList();
ArrayList<AudioPlayer> players = new ArrayList();

int maxPlayers = 120;

Minim minim;
MP3List list;
int bufferSize = 1024;

boolean initPhase = true;
float maxGain = -48;

void setup() {
  size(600, 500, P3D);
  minim = new Minim(this);


  println("Loading mp3 list..");
  list = new MP3List("D:\\- Party40\\database audio");
  println("done ("+list.size()+" files)");

  //  for (int i=0;i<maxPlayers;i++) {
  //    AudioPlayer p = loadNewFile();
  //   // p.setBalance(map(i,0,maxPlayers-1,-1,1));
  //  }
  MidiBus.list();
  input = new MidiBus(this, "VMeter 1.30 A", -1);
}





void mousePressed() {

  if (mouseButton==RIGHT) { 
    println("skipping");
    //for (int i=0;i<players.size();i++) {
    int i = (int)(random(players.size()));
    AudioPlayer player = (AudioPlayer)players.get(i);
    if (player.isPlaying()) {
      int timeRemaining = player.length() - player.position();
      println("rem "+timeRemaining);
      player.skip(timeRemaining/2);
    }
    //}
  }
}

int screenPosToPlayerIndex(float x) {
  return (int)map(x, 0, width, 0, maxPlayers);
}

float playerIndexToScreenPos(int index) {
  return map(index, 0, maxPlayers, 0, width);
}

int screenPosToPlayerRange(float y) {
  return (int)map(y, height, 0, 0, maxPlayers);
}


void updatePlayers() {
  //if (mouseHasMoved) {
  //mouseHasMoved = false;
  int centerIndex = screenPosToPlayerIndex(mouseX);
  int range = screenPosToPlayerRange(mouseY);
  int i0 = max(centerIndex, 0);
  int i1 = min(centerIndex+range, maxPlayers-1);

  float x0 = playerIndexToScreenPos(i0);
  float x1 = playerIndexToScreenPos(i1);

  rectMode(CORNERS);
  fill(0, 128, 255, 128);
  rect(x0, height/2-2, x1, height/2+2);

  if (!mousePressed) {
    i0=maxPlayers;
    i1=maxPlayers+1;
  }

  float percentageOfAll = (float)(i1-i0)/maxPlayers;
  //println(percentageOfAll);
  float onGain = map(percentageOfAll, 0, 1, maxGain, 0);
  
  for (int i=0;i<players.size();i++) {
    AudioPlayer player = (AudioPlayer)players.get(i);
    if (i<i0 || i>=i1) {
      player.setGain(-80);
    } 
    else {
      player.setGain(onGain);
    }
  }
  //}
}

void draw() {
  background(0);
  noStroke();



  if (initPhase) {
    if (players.size()<maxPlayers) {
      loadNewFile();
    } 
    else {
      Collections.sort(players, new Comparator() {
        public int compare(Object o0, Object o1) {
          AudioPlayer p0 = (AudioPlayer)o0;
          AudioPlayer p1 = (AudioPlayer)o1;
          int l0 = p0.length();
          int l1 = p1.length();
          if (l0<l1) {
            return -1;
          } 
          else if (l0>l1) {
            return 1;
          } 
          else {
            return 0;
          }
        }
      }
      );
      for (int i=0;i<players.size();i++) {
        //println(players.get(i).length());
        players.get(i).setBalance(map(i, 0, players.size()-1, -1, 1));
      }
      initPhase = false;
    }
  }




  // remove stopped files:
  //  for (int i=players.size()-1;i>=0;i--) {
  //    AudioPlayer player = (AudioPlayer)players.get(i);
  //    if (!player.isPlaying()) {
  //      players.remove(i);
  //    }
  //  }


  for (int i=0;i<players.size();i++) {
    AudioPlayer player = (AudioPlayer)players.get(i);
    long len = player.length();
    long pos = player.position();

    float y0 = map(len, 0, 6*60000, height, 20);
    float y1 = map(pos, 0, 6*60000, height, 20);
    float w = (float)width/maxPlayers;
    float x = i*w;

    if (player.isPlaying()) {
      fill(128);
    } 
    else {
      fill(128, 0, 0);
    }

    rectMode(CORNERS);
    rect(x, y0, x+w, height);
    if (player.isPlaying()) {
      fill(0, 128, 0);
      rect(x, y1, x+w, height);
    }
  }

  fill(255);
  text("Files playing: "+players.size(), 10, 20);

  updatePlayers();
}

