import ddf.minim.*;
import themidibus.*;
import java.util.List;
import java.util.Arrays;

String midiIn = "TASCAM US-122 MKII MIDI";
MidiBus myBus;
int pitchLowest = 28;
int pitchHighest = 103;
ArrayList notesHolding = new ArrayList();


AudioPlayer[] players;
int maxPlayers = pitchHighest-pitchLowest+1;

Minim minim;

MP3List list;


void setup() {
  size(600, 500, P3D);
  minim = new Minim(this);
  
  MidiBus.list();
  myBus = new MidiBus(this, midiIn, -1); // Create a new MidiBus with no input device and the default Java Sound Synthesizer as the output device.

  println("Loading mp3 list..");
  list = new MP3List("D:\\- Party40\\Audio");
  println("done ("+list.size()+" files)");

  players = new AudioPlayer[maxPlayers];
  for (int i=0;i<maxPlayers;i++) {
    loadNewFile(i);
  }
}

//String[] notes = new String[] {
//  "C","C#","D","D#","E",
//  "F","F#","G","G#","A","A#","B"
//};
//int noteName(int pitch) {
  //int octave = (pitch/12)-3;
  //String note = notes[pitch%12];
//  return pitch;
//}

void noteOn(int channel, int pitch, int velocity) {
  //String note = noteName(pitch);
  Integer p = new Integer(pitch);
  if (!notesHolding.contains(p)) {
    notesHolding.add(p);
  }
  players[pitch-pitchLowest].setGain(map(velocity,0,127,-48,0));
  println(notesHolding);
}

void noteOff(int channel, int pitch, int velocity) {
  Integer p = new Integer(pitch);
  notesHolding.remove(p);
  if (!sustainIsOn) {
    players[pitch-pitchLowest].setGain(-80);
  }
  println(notesHolding);
}


boolean sustainIsOn = false;
void controllerChange(int channel, int number, int value) {
  // sustain:
  if (number==64) {
    if (value>0) {
      sustainIsOn = true;
    } else if (sustainIsOn) {
      sustainIsOn = false;
      
      // all notes off except holding:
      println("notesHolding: "+notesHolding);
      
      for (int i=0;i<players.length;i++) {
        Integer playerPitch = new Integer(i + pitchLowest);
        if (notesHolding.indexOf(playerPitch)==-1) {
          players[i].setGain(-80);
        }
      }
      
    }
  }
  // Receive a controllerChange
//  println();
//  println("Controller Change:");
//  println("--------");
//  println("Channel:"+channel);
//  println("Number:"+number);
//  println("Value:"+value);
}

int screenPosToPlayerIndex(float x) {
  return (int)map(x, 0, width, 0, maxPlayers);
}

float playerIndexToScreenPos(int index) {
  return map(index, 0, maxPlayers, 0, width);
}



void draw() {
  background(0);
  noStroke();

  // remove stopped files:
//  for (int i=players.size()-1;i>=0;i--) {
//    AudioPlayer player = (AudioPlayer)players.get(i);
//    if (!player.isPlaying()) {
//      players.remove(i);
//    }
//  }


  for (int i=0;i<players.length;i++) {
    AudioPlayer player = players[i];
    long len = player.length();
    long pos = player.position();

    float y0 = map(len, 0, 6*60000, height, 20);
    float y1 = map(pos, 0, 6*60000, height, 20);
    float w = (float)width/maxPlayers;
    float x = i*w;

    if (player.getGain()<-48) {
      fill(128);
    } else {
      if (player.isPlaying()) {
        fill(0,64,0);
      } else {
        fill(128,0,0);
      }
    }
    
    rectMode(CORNERS);
    rect(x, y0, x+w, height);
    if (player.isPlaying()) {
      fill(0, 128, 0);
      rect(x, y1, x+w, height);
    }
  }

  fill(255);

}



