class MP3List {
  
  ArrayList list = new ArrayList();
  
  MP3List(String rootPath) {
    recurse(new File(rootPath));
  }
  
  int size() {
    return list.size();
  }
  
  File get(int index) {
    return (File)list.get(index);
  }
  
  void recurse(File dir) {
    
    File[] files = dir.listFiles();
    for (int i=0;i<files.length;i++) {
      
      if (files[i].isDirectory()) {
        recurse(files[i]);
      }
      else if (files[i].isFile() && files[i].getName().toLowerCase().endsWith(".mp3")) {
        list.add(files[i]);
      }
      
    }
    
  }
  
}
