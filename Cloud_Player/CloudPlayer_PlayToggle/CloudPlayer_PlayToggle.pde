import ddf.minim.*;
import java.util.Collections;
import java.util.Comparator;

AudioPlayer[] players;

int maxPlayers = 100;

Minim minim;
MP3List list;
int bufferSize = 1024;

void setup() {
  size(600, 500, P3D);
  minim = new Minim(this);


  println("Loading mp3 list..");
  list = new MP3List("D:\\- Party40\\Audio");
  println("done ("+list.size()+" files)");

  players = new AudioPlayer[maxPlayers];
  for (int i=0;i<maxPlayers;i++) {
    loadNewFile(i);
  }
}




int screenPosToPlayerIndex(float x) {
  return (int)map(x, 0, width, 0, maxPlayers);
}

float playerIndexToScreenPos(int index) {
  return map(index, 0, maxPlayers, 0, width);
}


void updatePlayers() {
  //if (mouseHasMoved) {
  //mouseHasMoved = false;
  int centerIndex = screenPosToPlayerIndex(mouseX);
  AudioPlayer player = players[centerIndex];
  
  if (mousePressed) {
    player.setGain(0);
  } else {
    player.setGain(-80);
  }
  
}

void draw() {
  background(0);
  noStroke();

  // remove stopped files:
//  for (int i=players.size()-1;i>=0;i--) {
//    AudioPlayer player = (AudioPlayer)players.get(i);
//    if (!player.isPlaying()) {
//      players.remove(i);
//    }
//  }


  for (int i=0;i<players.length;i++) {
    AudioPlayer player = players[i];
    long len = player.length();
    long pos = player.position();

    float y0 = map(len, 0, 6*60000, height, 20);
    float y1 = map(pos, 0, 6*60000, height, 20);
    float w = (float)width/maxPlayers;
    float x = i*w;

    if (player.getGain()<-48) {
      fill(128);
    } else {
      if (player.isPlaying()) {
        fill(0,64,0);
      } else {
        fill(128,0,0);
      }
    }
    
    rectMode(CORNERS);
    rect(x, y0, x+w, height);
    if (player.isPlaying()) {
      fill(0, 128, 0);
      rect(x, y1, x+w, height);
    }
  }

  fill(255);

  updatePlayers();
}



