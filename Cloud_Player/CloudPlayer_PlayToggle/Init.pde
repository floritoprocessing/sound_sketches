void loadNewFile(int arrIndex) {
  // get a new random file
//  File file = null;
//  do {
//    int index = (int)(Math.random()*list.size());
//    file = list.get(index);
//  } 
//  while (filesPlaying.contains (file));

  int index = (int)(Math.random()*list.size());
  File file = list.get(index);

  // load file
  //println("Loading "+file.getName());
  AudioPlayer player = minim.loadFile(file.getAbsolutePath());
  players[arrIndex] = player;
  player.play();
  player.setGain(-80);
  //println(player.getControls());
  //player.setVolume(0);
  //player.shiftVolume(1,0,10000);
}
