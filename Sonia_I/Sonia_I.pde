Sample[] sine; int sampleNr;
float halftone=pow(2,1/12.0);

void setup() {
  size(400,400);
  Sonia.start(this,44100);
  sine=new Sample[64];
  for (int i=0;i<12;i++) {
    sine[i]=new Sample(44100);
    float[] data=new float[44100];
    float oneCycle=TWO_PI/44100.0;
    float freq=440*pow(halftone,i);
    for (int s=0;s<44100;s++) {
      float amp=1.0-s/44100.0;
      data[s]=amp*sin(s*oneCycle*freq);
    }
    sine[i].write(data);
  }
  sampleNr=0;
}

void loop() {
}

void mousePressed() {
  sine[sampleNr].play();
  sampleNr++;
}

public void stop(){ 
  Sonia.stop(); 
//  super.stop(); 
} 
